# -*- coding: utf-8 -*-
import math, datetime
import pandas as pd

vacant = lambda value: value is None
occupied = lambda value: not value is None

def load(target):
    splited = {}
    count = 0
    
    for file_path in target:
        df = pd.read_excel(file_path)
        df = df.drop(columns = '标题')

        for index, row in df.iterrows():
            date = row['日期'][-8:].replace('-', '')

            if date not in splited: splited[date] = {}
            code = row[0]
            for key in row.index:
                if key in {'企业总评分', '日期'}: continue
                if key not in splited[date]: splited[date][key] = [None] * 3000
                splited[date][key][code - 1001] = row[key]

        # splited[date] = splited[date].append(row.drop(columns = "日期"), ignore_index = True)

        count += 1
        # print(count)
        # if count == 100:
        #     break
    # print(splited.keys())
    print('load done')
    return splited

def numberify(value):
    # {'NoneType', 'str', 'float', 'int'}
    if isinstance(value, str):
        value = value.replace('正无穷大万亿', '--')
        if '--' in value:
            return None
        else:
            return eval(value.replace('亿', '*100000000').replace('万', '*10000').replace('%', '*0.01').replace(',', ''))
    elif isinstance(value, float):
        if math.isnan(value):
            return None
        else:
            return value
    else:
        return value

def complete(*arrays):
    row = set([len(array) for array in arrays])
    assert len(row) == 1
    row = row.pop()
    result = [None] * row
    for index in range(row):
        for array in arrays:
            if occupied(array[index]):
                result[index] = array[index]
                break
    return result
    
    # assert len(array_one) == len(array_two)
    # for index, value in enumerate(array_two):
    #     if vacant(array_one[index]):
    #         array_one[index] = value
    # return array_one

def compute(*arguments):
    row = set([len(argument) for argument in arguments if isinstance(argument, list)])
    assert len(row) == 1
    row = row.pop()
    result = [None] * row
    for index in range(row):
        compution = ''
        for argument in arguments:
            if isinstance(argument, list):
                if occupied(argument[index]):
                    compution += str(argument[index])
                else:
                    compution = ''
                    break
            else:
                compution += argument
        if compution:
            try:
                result[index] = eval(compution)
            except ZeroDivisionError:
                pass
    return result

def average(*arrays):
    
    def _average_(_array_):
        if not _array_: return None
        _array_ = list(filter(occupied, _array_))
        if not _array_: return None
        return 1.0 * sum(_array_) / len(_array_)
    
    if len(arrays) == 1: return _average_(arrays[0])
    row = set([len(array) for array in arrays])
    assert len(row) == 1
    row = row.pop()
    result = [_average_([array[index] for array in arrays]) for index in range(row)]
    return result

def discard(array): # too much missing
    redundancy = list(filter(vacant, array))
    return len(redundancy) > 0.5 * len(array)

def arggragate(result):
    output = {}

    for date in result:
        data = result[date]
        year = datetime.datetime.strptime(date, '%y%m%d').strftime('%Y')
        if year not in output: output[year] = {}
        for attribute in data:
            if attribute not in output[year]: output[year][attribute] = []
            output[year][attribute].append(data[attribute])

    for year in output:
        data = output[year]
        for attribute in data:
            # print(year, attribute)
            output[year][attribute] = data[attribute][0] if len(data[attribute]) == 1 else average(*data[attribute])

    return output

def indexify(data):
    data.update({'企业编号': range(1001, 4001)})
    return data


def insure(result, enable = True):
    
    for date in result:
        # RuntimeError: dictionary changed size during iteration
        meanless = [attribute for attribute in result[date] if discard(result[date][attribute])]
        for attribute in meanless: result[date].pop(attribute, None)

        for attribute in result[date]:
            for index, value in enumerate(result[date][attribute]):
                if not enable:
                    continue
                if occupied(value):
                    continue
                if vacant(value):
                    # average value from a certain enterprise in different time
                    value = average([result[_date_][attribute][index] for _date_ in result if attribute in result[_date_]]) 
                if vacant(value):
                    # average value from all enterprises in a certain time
                    value = average(result[date][attribute])
                if occupied(value):
                    result[date][attribute][index] = value
    
    meanless = [date for date in result if not any(result[date])]
    for date in meanless: result.pop(date, None)

    return result