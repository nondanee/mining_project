# -*- coding: utf-8 -*-
import pandas as pd
import os
import datetime
import math
import utility

# print(df.columns)
# print(df.index)
# print(df.iloc[0])

def process_base(data):
    data = {attribute: list(map(utility.numberify, data[attribute])) for attribute in data}
    return data

def process_profile(data):
    data = process_base(data)

    try:
        if '归属母公司所有者净利润(元)' in data:
            data['归属净利润(元)'] = utility.complete(data['归属母公司所有者净利润(元)'], data['归属净利润(元)'])
        else:
            pass
        data.pop('归属母公司所有者净利润(元)', None)
    except KeyError as error:
        print(error)

    try:
        if '营业收入(元)' in data:
            data['营业收入(元)'] = utility.complete(data['营业收入(元)'], data['营业总收入(元)'])
        else:
            data['营业收入(元)'] = data['营业总收入(元)']
        data.pop('营业总收入(元)', None)
    except KeyError as error:
        print(error)

    try:
        business_profit = utility.compute(data['营业收入(元)'], '-', data['营业成本(元)'], '-', data['销售费用(元)'], '-', data['财务费用(元)'], '-', data['管理费用(元)'], '-', data['资产减值损失(元)'], '+', data['投资收益(元)'])
        data['营业利润(元)'] = utility.complete(data['营业利润(元)'], business_profit)
    except KeyError as error:
        print(error)

    try:
        raw_profit_computed = utility.compute(data['营业收入(元)'], '-', data['营业成本(元)'])
        data['毛利润(元)'] = utility.complete(data['毛利润(元)'], raw_profit_computed)
    except KeyError as error:
        print(error)

    try:
        raw_profit_rate_computed = utility.compute(data['毛利润(元)'], '*1.0/', data['营业收入(元)'])
        data['毛利率(%)'] = utility.complete(data['毛利率(%)'], raw_profit_rate_computed)
    except KeyError as error:
        print(error)

    try:
        real_tax_rate_computed = utility.compute(data['所得税(元)'], '*1.0/', data['利润总额(元)'])  # 实际税率(%) = 所得税(元) / 利润总额(元) checked
        data['实际税率(%)'] = utility.complete(data['实际税率(%)'], real_tax_rate_computed)
    except KeyError as error:
        print(error)
    
    return data

def process_assets(data):
    data = process_base(data)

    try:
        total_current_assets_computed = utility.compute(data['资产:货币资金(元)'], '+', data['资产:应收账款(元)'], '+', data['资产:其它应收款(元)'], '+', data['资产:存货(元)'])
        data['资产:流动资产合计(元)'] = utility.complete(data['资产:流动资产合计(元)'], total_current_assets_computed)
    except KeyError as e:
        print(e)

    try:
        data['流动比率'] = utility.complete(data['流动比率'], data['流动比率(重)'])
        data.pop('流动比率(重)', None)
    except KeyError as e:
        print(e)

    try:
        total_assets_computed = utility.compute(data['资产:流动资产合计(元)'], '+', data['资产:长期股权投资(元)'], '+', data['资产:固定资产(元)'], '+', data['资产:无形资产(元)'])
        data['资产:资产总计(元)'] = utility.complete(data['资产:资产总计(元)'], total_assets_computed)
    except KeyError as e:
        print(e)

    try:
        total_liabilities_computed = utility.compute(data['负债:流动负债合计(元)'], '+', data['负债:长期负债合计(元)'])
        data['负债:负债合计(元)'] = utility.complete(data['负债:负债合计(元)'], total_liabilities_computed)
    except KeyError as e:
        print(e)

    try:
        shareholders_equity_computed = utility.compute(data['资产:资产总计(元)'], '-', data['负债:负债合计(元)'])
        data['权益:股东权益合计(元)'] = utility.complete(data['权益:股东权益合计(元)'], shareholders_equity_computed)
        shareholders_equity_computed = utility.compute(data['权益:实收资本(或股本)(元)'], '+', data['权益:资本公积金(元)'], '+', data['权益:盈余公积金(元)'])
        data['权益:股东权益合计(元)'] = utility.complete(data['权益:股东权益合计(元)'], shareholders_equity_computed)
    except KeyError as e:
        print(e)

    try:
        asset_liability_ratio_computed = utility.compute(data['资产:资产总计(元)'], '*1.0/', data['负债:负债合计(元)'])
        data['资产负债率(%)'] = utility.complete(data['资产负债率(%)'], asset_liability_ratio_computed)
    except KeyError as e:
        print(e)

    try:
        current_debt_ratio_computed = utility.compute(data['负债:流动负债合计(元)'], '*1.0/', data['负债:负债合计(元)'])
        data['流动负债/总负债(%)'] = utility.complete(data['流动负债/总负债(%)'], current_debt_ratio_computed)
    except KeyError as e:
        print(e)

    try:
        current_ratio_computed = utility.compute(data['资产:流动资产合计(元)'], '*1.0/', data['负债:流动负债合计(元)'])
        data['流动比率'] = utility.complete(data['流动比率'], current_ratio_computed)
    except KeyError as e:
        print(e)

    try:
        quick_ratio_computed = utility.compute('(', data['资产:流动资产合计(元)'], '-', data['资产:存货(元)'], ')', '*1.0/', data['负债:流动负债合计(元)'])
        data['速动比率'] = utility.complete(data['速动比率'], quick_ratio_computed)
    except KeyError as e:
        print(e)

    return data

def process_cross_date(data):

    def addition(dates, attributes):
        compution = []

        for date, attribute in zip(dates, attributes):
            compution.append(data[date][attribute])
            compution.append('+')
        compution.pop()

        return utility.compute(*compution)

    for date in data:
        _date_ = datetime.datetime.strptime(date, '%y%m%d')
        
        target = _date_.replace(year = _date_.year - 1).strftime('%y%m%d')

        for pair in [
            ['营业收入(元)', '营业总收入同比增长(%)'],
            ['归属净利润(元)', '归属净利润同比增长(%)'],
            ['扣非净利润(元)', '扣非净利润同比增长(%)']
        ]:

            try:
                increased_year_on_year_computed = utility.compute('(', data[date][pair[0]], '-', data[target][pair[0]], ')', '*1.0/', data[target][pair[0]])
                data[date][pair[1]] = utility.complete(data[date][pair[1]], increased_year_on_year_computed)
            except KeyError as error:
                print('year_on_year failed', date, target, error)
                pass

        checkpoints = ['1231', '0930', '0630', '0331']
        index = checkpoints.index(date[2:])
        year = [_date_.replace(year = _date_.year - delta).strftime('%y') for delta in range(3)]
        current_round = list(map(lambda date: year[0] + date, checkpoints[index:])) + list(map(lambda date: year[1] + date, checkpoints[0:index]))
        previous_round = list(map(lambda date: year[1] + date, checkpoints[index:])) + list(map(lambda date: year[2] + date, checkpoints[0:index]))

        for pair in [
            ['营业收入(元)', '营业总收入滚动环比增长(%)'],
            ['归属净利润(元)', '归属净利润滚动环比增长(%)'],
            ['扣非净利润(元)', '扣非净利润滚动环比增长(%)']
        ]:

            try:
                current_round_sum = addition(current_round, [pair[0]] * len(current_round))
                previous_round_sum = addition(previous_round, [pair[0]] * len(previous_round))

                increased_round_on_round_computed = utility.compute('(', current_round_sum, '-', previous_round_sum, ')' '*1.0/', previous_round_sum)
                data[date][pair[1]] = utility.complete(data[date][pair[1]], increased_round_on_round_computed)
            except KeyError as error:
                print('round_on_round failed', current_round, previous_round, error)
                pass

    return data


if __name__ == '__main__':

    target = [
        '上市公司财务信息-利润表.xlsx',
        '上市公司财务信息-成长能力指标.xlsx',
        '上市公司财务信息-盈利能力指标.xlsx'
    ]

    # target = [
    #     '上市公司财务信息-资产负债表.xlsx',
    #     '上市公司财务信息-财务风险指标.xlsx',
    #     '上市公司财务信息-运营能力指标.xlsx'
    # ]

    # target = [
    #     '上市公司财务信息-现金流量表.xlsx',
    #     '上市公司财务信息-每股指标.xlsx'
    # ]

    target = [os.path.join('dataset', name) for name in target]

    splited = utility.load(target)
    # splited = {date: process_profile(splited[date]) for date in splited}
    
    for date in splited:
        print(date)
        splited[date] = process_base(splited[date])

    splited = process_cross_date(splited)
    
    output = utility.arggragate(splited)

    output = utility.insure(output)

    for year in output: pd.DataFrame(utility.indexify(output[year])).to_excel('{}-{}.xlsx'.format('利润', year), index = False)